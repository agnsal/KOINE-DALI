DALI extended framework, compatible with Docker, Redis and other technologies.  
You have to put DALI subfolders into KOINE-DALI/DALI/ to make it work.  
You can find DALI at: https://github.com/AAAI-DISIM-UnivAQ/DALI.  
If you need more tools, see also: https://github.com/agnsal.